import React from 'react';
import './styles/header.css';
import {Link} from 'react-router-dom';

import NavComponent from './navbar'

const Header =(props) =>
{
    const logo = ()=>(
            <Link to="/" className="logo">
                <img alt =" logo" src="/images/logo.png" />
            </Link>
    )
    const backgroundColor = "#FFF"

    return (

        <div className="header" style={{backgroundColor: props.backgroundColor}}>
            {logo()}
           <NavComponent backgroundColor={props.backgroundColor}/>
        </div>
    )
}

export default Header;
