import React from 'react';
import styles from './formfieldscampusbusinessmanager.css';

const FormFieldsCampusBusinessManager=(props) =>{

    const renderFields=()=>{
        const formArray=[];
        for(let elementName in props.formdata){
            formArray.push({
                id:elementName,
                settings: props.formdata[elementName]
    })
        }
        return formArray.map((item,i)=>{
            return(
                <div  className={styles.form_element}key={i}>
                {renderTempletes(item)}
                </div>
            )
        })

    }

    const showLabel=(show,label)=>{
    return show ?
    <label>
    {label}
    </label>
    :null
    }

    const changeHandler=(event,id,blur)=>{
    const newState= props.formdata;
    newState[id].value=event.target.value;
    if (blur){
    let validData=validate(newState[id])
        newState[id].valid=validData[0];
        newState[id].validationMessage=validData[1];
    }
    newState[id].touched=true;
    props.change(newState)
    }
const validate =(element)=>
{
    let error=[true,'']

   
         if(element.validation.email){
                 const valid=/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i.test(element.value);
                  const message=`${!valid ? 'Please enter valid email':''}`;
                  error= !valid ? [valid,message]: error
         }
        if(element.validation.phone){
             const valid= /^[0-9]{10}/i.test(element.value);
              const message=`${!valid ? 'Please enter valid mobile number':''}`;
    //<input type="number" pattern="[0-9]*" inputmode="numeric">
              error= !valid ? [valid,message]: error
     }
     



    if(element.validation.required){
        const valid= element.value.trim() !=='';
        const message= `${!valid ? 'This field is required':''}`
        error=!valid?[valid,message]:error
    }    
    return error;
}

    const showValidation=(data)=>{
        let errorMessage=null;
        if(data.validation && !data.valid){
            errorMessage=(
                <div className={styles.label_error}>
                    {data.validationMessage}
                 </div>   
            )
        }
        return errorMessage;
    }
        const renderTempletes=(data)=>{
        let values=data.settings;
        let formTemplate='';
        switch(values.element)
        {
            case('input'):
            formTemplate=(
            <div>
                <div className={styles.label}>
                <p>
                {showLabel(values.label,values.labelText) }
                
                </p>
                </div>
                <input
                {...values.config}
                value={values.value}
                onBlur={
                    (event)=> changeHandler(event,data.id,true)
                }
                onChange={
                    (event)=> changeHandler(event,data.id,false)
                }
                />
                {showValidation(values)}

                </div>
                        )
                                    break;
default:
formTemplate:null
        }
        return formTemplate;
    }
    return(
        <div>
            {renderFields()}
            </div>
    )
}
export default FormFieldsCampusBusinessManager;