import React, {Component} from 'react';
import styles from './styles/register.css';
//import FormField from '../Widgets/FormFields/formfields';
import FormFieldsRegister from './Widgets/FormFields/formfieldsregister';

import {Link} from 'react-router-dom';

import GoogleLogin2 from './GoogleLogin2.js';


class PopUpRegister extends Component {

    constructor(props) {
        super(props)
    this.state={
        registerError:'',
        loading:false,
        isRegistered: false,
        isSignedIn: false,
        registrationStatus: "",

    formdata:{
        name:{
            element:'input',
            value:'',
            label:false,
            labelText:'Name *',
        config:{
            name:'name_input',
            type:'text',
            placeholder:'Name *'
        },
    validation:{
    required: true,
    name:true
},
valid:false,
touched:false,
validationMessage:''
    },
    email:{
        element:'input',
        value:'',
        label:false,
            labelText:'Email *',
        config:{
            name:'email_input',
            type:'email',
            placeholder:'Email address *',


        },
validation:{
    required: true,
    email:true
},
valid:false,
touched:false,
validationMessage:''
    },
    phone:{
        element:'input',
        value:'',
        label:false,
            labelText:'Phone no. * ',
        config:{
            name:'phone_input',
            type:'tel',
            placeholder:'Phone no. *',
            maxLength: 10,

        },
validation:{
    required: true,
    name:true
},
valid:false,
touched:false,
validationMessage:''
    },

    companies:{
        element:'input',
        value:'',
        label:false,
            labelText:'Companies',
        config:{
            name:'company_input',
            type:'text',
            placeholder:'Companies'


        },
validation:{
    required: false,
    name:true
},
valid:true,
touched:false,
validationMessage:''
    },
    password:{
        element:'input',
        value:'',
        label:false,
        labelText:'Password *',

        config:{
            name:'password_input',
            type:'password',
            placeholder:'Password *',


        },
        validation:{
            required: true,
            password:false
        },
        valid:false,
        touched:false,
        validationMessage:''
            }

    }
    }

    this.SubmitForm = this.SubmitForm.bind(this);
    this.onGoogleLogin=this.onGoogleLogin.bind(this);
}
    updateForm=(newState)=>{
        this.setState({
            formdata:newState
        })
    }

     onGoogleLogin=(data)=>{


    }

    SubmitForm=(event)=>{

    }

    render(){
  /*      if(this.state.isSignedIn) {
            return(
                <div className="popup">
                <img onClick={this.props.handleCloseButton} className="popup_close_button" src="images/closebutton.png" alt="close"/>
                <div className="register_popup">
                   <div className="success_text">
                   <h1 align="center">Thank You!</h1>
                    {this.props.placementProgram?<p align="center">You have successfully registered with us for the  Placement Program.Our team will contact you soon.</p>:
                                                  <p align="center">You have successfully registered with us for the mock interview.Our team will contact you soon.</p>}
                </div>
                </div>
                </div>
            )
        } */

        return(
            <div className="popup" style={{backgroundImage:"url('./road.jpg')"}}>
            <div className="register_popup">
            <h2 align= "center" style={{color:"#000000"}}>Register with us</h2>
            <div align="center">
            <form autoComplete="off" onSubmit={this.SubmitForm}>
            <FormFieldsRegister
                formdata={this.state.formdata}
                change={(newState)=>this.updateForm(newState)}
                onBlur={(newState)=>this.updateForm(newState)}
                />
                <p align="center">{this.state.registrationStatus}</p>
                <p align="center">
                <button type="Submit" className="register_button"> Register</button>
                </p>
                </form>
                </div>
                <p className="p2"> Already have a account?  <span onClick={this.props.handleLoginClick} style={{color: "#000000", fontWeight: "bold",
                                                                                                                cursor: "pointer"}}> Login Here</span>
                </p>
                 <div align="center"> <GoogleLogin2
                 onGoogleLogin={(data)=>this.onGoogleLogin(data)}
                 />
                </div>

                </div>
              </div>
        )
    }
}

export default PopUpRegister;
