import React, {Component} from 'react';
import styles from './styles/signin.css';
import styles2 from './styles/register.css';
import FormField from './Widgets/FormFields/formfields';
import {Link} from 'react-router-dom';
import GoogleLogin2 from './GoogleLogin2.js';

class PopUpSignIn extends Component {
     constructor(props) {
    super(props)
   this.state={
        registerError:'',
        loading:false,
        registrationStatus: "",
formdata:{
    email:{
        element:'input',
        value:'',
        config:{
            name:'email_input',
            type:'email',
            placeholder:'Enter your email',
            label:true,
            labelText:'Email',

        },
validation:{
    required: true,
    email:true
},
valid:false,
touched:false,
validationMessage:''
    },
    password:{
        element:'input',
        value:'',

        config:{
            name:'password_input',
            type:'password',
            placeholder:'Enter your password',
            label:true,
            labelText:'Password',

        },
        validation:{
            required: true,
            password:false
        },
        valid:false,
        touched:false,
        validationMessage:''
            }

    }

}
this.SubmitForm=this.SubmitForm.bind(this)
}
    updateForm=(element)=>{
        const newFormdata={
            ...this.state.formdata
        }
        const newElement = {
            ...newFormdata[element.id]
        }
        newElement.value= element.event.target.value;
        if (element.blur){
            let validData=this.validate(newElement);
            newElement.valid=validData[0];
            newElement.validationMessage=validData[1];



        }
        newElement.touched=element.blur;
        newFormdata[element.id]=newElement;
        this.setState({
            formdata:newFormdata
        })
    }



    validate = (element) =>{
        let error =[true,''];

        if(element.validation.email){
            const valid=/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i.test(element.value);
             const message=`${!valid ? 'Please enter valid email':''}`;
             error= !valid ? [valid,message]: error
         }

         if(element.validation.password){
            const valid= /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/i.test(element.value);
             const message=`${!valid ? 'Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters':''}`;
            error= !valid ? [valid,message]: error
        }

        if(element.validation.required){
            const valid=element.value.trim() !=='';
            const message=`${!valid ? 'This field is required':''}`;
            error= !valid ? [valid,message]: error
        }

        return error;
        }



        SubmitForm=(event)=>{
            event.preventDefault();

        }


    render(){
        if(this.state.isSignedIn) {
            return(
                <div className="popup">
                <img onClick={this.props.handleCloseButton} className="popup_close_button" src="images/closebutton.png" alt="close"/>
                <div className="register_popup">
                   <div className="success_text">
                   <h1 align="center">Thank You!</h1>
                   {this.props.placementProgram?<p align="center">You have successfully registered with us for the  Placement Program.Our team will contact you soon.</p>:
                                                  <p align="center">You have successfully registered with us for the mock interview.Our team will contact you soon.</p>}
                </div>
                </div>
                </div>
            )
        }
        return(
            <div className="popup" style={{backgroundImage:"url('./road.jpg')"}}>

            <div className="register_popup">

            <h2 className="h2" style={{color:"black"}}> Login Here</h2>
            <div align="center">
            <form autocomplete="off" onSubmit={this.SubmitForm}>

        <FormField

            id={'email'}
            formdata={this.state.formdata.email}
            change={(element)=>this.updateForm(element)}/>

        <FormField
            id={'password'}
            formdata={this.state.formdata.password}
            change={(element)=>this.updateForm(element)}
        />

       <p className="p1">
        <button type="Submit" className="login_button" > Login</button>
       </p>

        </form>

        <div align="center"> <GoogleLogin2
        onGoogleLogin={(data)=>this.onGoogleLogin(data)}
        />
       </div>

        <p>{this.state.loginMessage}</p>
                <p className="p2"> Dont have a account?  <span onClick={this.props.handleCreateAccountClick} className="login"> Create Account </span>
                </p>
                </div>

                </div>
                </div>
        )
    }
}

export default PopUpSignIn;
