import React, { Component } from 'react';
import InputRange from 'react-input-range';

import './slider.css';
import 'react-input-range/lib/css/index.css';
import ReactDOM from 'react-dom';
import Cirelement from './cirElement.js';
import appConfig from './../../appConfig.js'

class slider extends React.Component {
  constructor(props) {
    super(props);

    appConfig.packageRange="4 lacs - 7 lacs"

    appConfig.serviceId="PPLevel3"


	this.state = {
      value: { min: 4, max: 7  },
	  hover:{min:"4",max:"7"},
      price: 4999,
	  rangeSelected:"4 lacs - 7 lacs",
	  priceList:[
	  {
		 id:"PPLevel4",
         value:{ min: 3, max: 5 },
         price: 3499
	  },
	  {
		id:"PPLevel3",
        value:{min: 3, max: 7 },
		price: 4999
	  },
	  {
		  id: "PPLevel2",
		  value: {min: 3, max: 10},
		  price: 5999
	  },
	  {
		  id: "PPLevel1",
		  value: {min: 3, max: 11},
		  price: 8499

	  }

	]

    };
	this.onChangeval=this.onChangeval.bind(this)

  }


  onChangeval(value){
      this.setState({value:value})
	  if(value.max===11)
	  {
			this.setState({rangeSelected:value.min+" lacs - above 10 lacs"})
	  } else
	  this.setState({rangeSelected:value.min+" lacs - "+value.max+" lacs"})
      this.setState({hover:{min:value.min.toString(), max:value.max.toString()}});
	  const rows = this.state.priceList;
	  const row = rows.find(row => row.value.min <= value.min && row.value.max >= value.max);
	  if(row!==undefined)
	  this.setState({price:row.price})

      else
	  this.setState({price:3546})
	  appConfig.packageRange=this.state.rangeSelected
	  appConfig.packagePrice = this.state.price
    appConfig.serviceId=row.id
   }

   getStyle(id){
	   if(id===this.state.hover.min ||id===this.state.hover.max)
	   {
		   return "cirrange";}
	   else
		   return "";
   }

  render() {
    return (

	<div className="swrapper">
	<div style={{width: '95%',marginBottom: '4%', marginLeft: '7%'}}>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='3' getStyle={this.getStyle('3') } style={{}} value='3'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='4' getStyle={this.getStyle('4') }style={{}} value='4'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='5' getStyle={this.getStyle('5') }style={{}} value='5'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='6' getStyle={this.getStyle('6') }style={{}} value='6'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='7' getStyle={this.getStyle('7') }style={{}} value='7'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='8' getStyle={this.getStyle('8') }style={{}} value='8'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='9' getStyle={this.getStyle('9')} style={{}} value='9'/></div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='10' getStyle={this.getStyle('10') } style={{}}value='10'/>
	</div>
	<div style={{display:'inline-block',margin:'auto',width:'11%',height:'auto'}}>
	<Cirelement id='11' getStyle={this.getStyle('11') } style={{}}value='10+'/>
	</div>

	</div>

	<div style={{width: '90%', marginLeft:'auto', marginRight: 'auto'}}>
      <InputRange
	  formatLabel={value => ``}
        maxValue={11}
        minValue={3}
        value={this.state.value}
        onChange={(value) => this.onChangeval( value )} />
	</div>
		<div className='info'>
  <div className='infoRange'>Your Dream Package range <div className='range'>{this.state.rangeSelected}</div></div>
  <div className='infoPrice'><center>Program Price<div className='des'>A small price can get you your dream company</div><div className='price'>Rs. {this.state.price} only</div><div className='des'>*Enroll now to get exciting discounts/installments</div></center></div>

	</div>
		</div>
    );
  }
}


export default slider;
