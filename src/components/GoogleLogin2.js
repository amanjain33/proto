import React, { Component } from 'react';




class GoogleLogin2 extends Component{
    constructor(props) {
        super(props)
    }

     componentDidMount(){

            let e = document.createElement("script");
            e.type = "text/javascript";
            e.async = true;
            e.src = "https://apis.google.com/js/api:client.js";
            let t = document.getElementsByTagName("script")[0];
            if (!t) {
    document.appendChild(e)
  } else {
    t.parentNode.appendChild(e)
  }
      e.onload = () => {window.gapi.load('auth2', () => {
      if (!window.gapi.auth2.getAuthInstance()) {
        window.gapi.auth2.init({
          client_id:"dsgsgw4tw45tsdgsg4twtggetw"

	  });}
	  });
               console.log('library loaded');

	  }

    }

     googleLogin = () => {
       const GoogleAuth = window.gapi.auth2.getAuthInstance();
         GoogleAuth.signIn().then(()=>{
        const GoogleUser=GoogleAuth.currentUser.get();
        const userData={'authType':'google','name':GoogleUser.getBasicProfile().getName(),'email':GoogleUser.getBasicProfile().getEmail()};
	     const userDataJson=JSON.stringify(userData);
       this.props.onGoogleLogin(userData);


          console.log(userDataJson);},()=>{console.log('error')});
        }

 render(){
        return(
            <button className="loginBtn loginBtn--google"  title="google login" alt="google" onClick={ () => this.googleLogin() } >
  Login with Google
</button>

        )
    }
}
export default GoogleLogin2;
